﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace SzyfrowanieIDeszyfrowanie
{
    static class Decryption
    {
        //public static string DecryptionFile_AES(byte[] fileToDecryption, string key, string path, string filename, CipherMode mode)
        //{
        //    string returnstring = "Deszyfrowanie zakończone powodzeniem!";
        //    string filepath = path + @"\" + filename;
        //    try
        //    {
        //        using (var rijndael_AES = new RijndaelManaged())
        //        {
        //            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
        //            rijndael_AES.BlockSize = 128;
        //            rijndael_AES.Key = pdb.GetBytes(32);
        //            rijndael_AES.IV = pdb.GetBytes(16);
        //            rijndael_AES.Mode = mode;
        //            using (var ms = new MemoryStream())
        //            {
        //                using (var cs = new CryptoStream(ms, rijndael_AES.CreateDecryptor(), CryptoStreamMode.Write))
        //                {
        //                    cs.Write(fileToDecryption, 0, fileToDecryption.Length);
        //                    cs.Close();
        //                    if (!File.Exists(filepath))
        //                    {
        //                        using (var binarywrite = new BinaryWriter(File.Open(filepath, FileMode.Create)))
        //                        {
        //                            binarywrite.Write(ms.ToArray());
        //                            binarywrite.Flush();
        //                            binarywrite.Close();
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        returnstring = ex.Message;
        //    }
        //    return returnstring;
        //}

        public static string DecryptionFile_AES(byte[] fileToDecryption, string key, string path, string filename, CipherMode mode)
        {
            string returnstring = "Deszyfrowanie zakończone powodzeniem!";
            string filepath = path + @"\" + filename;
            try
            {
                using (Aes decryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    decryptor.BlockSize = 128;
                    decryptor.Key = pdb.GetBytes(32);
                    decryptor.IV = pdb.GetBytes(16);
                    decryptor.Mode = mode;
                    using (MemoryStream ms = new MemoryStream(fileToDecryption))
                    {
                        using (CryptoStream cs = new CryptoStream(ms, decryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(fileToDecryption, 0, fileToDecryption.Length);
                            cs.Close();
                            if (!File.Exists(filepath))
                            {
                                using (var binarywrite = new BinaryWriter(File.Open(filepath, FileMode.Create)))
                                {
                                    binarywrite.Write(ms.ToArray());
                                    binarywrite.Flush();
                                    binarywrite.Close();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                returnstring = ex.Message;
            }
            return returnstring;
        }

        public static string DecryptionFile_DES(byte[] fileToDecryption, string key, string path, string filename, CipherMode mode)
        {
            string returnstring = "Deszyfrowanie zakończone powodzeniem!";
            string filepath = path + @"\" + filename;
            try
            {
                using (var decryptor = DES.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    decryptor.BlockSize = 64;
                    decryptor.Key = pdb.GetBytes(8);
                    decryptor.IV = pdb.GetBytes(8);
                    decryptor.Mode = mode;
                    using (MemoryStream ms = new MemoryStream(fileToDecryption))
                    {
                        using (CryptoStream cs = new CryptoStream(ms, decryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(fileToDecryption, 0, fileToDecryption.Length);
                            cs.Close();
                            if (!File.Exists(filepath))
                            {
                                using (var binarywrite = new BinaryWriter(File.Open(filepath, FileMode.Create)))
                                {
                                    binarywrite.Write(ms.ToArray());
                                    binarywrite.Flush();
                                    binarywrite.Close();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                returnstring = ex.Message;
            }
            return returnstring;
        }
    }
}
