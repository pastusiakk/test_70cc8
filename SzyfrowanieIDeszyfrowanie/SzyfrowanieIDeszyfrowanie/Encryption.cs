﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace SzyfrowanieIDeszyfrowanie
{
    static class Encryption
    {
        //public static string EncryptionFile_AES(byte[] fileToEncryption, string key, string path, string filename, CipherMode mode)
        //{
        //    string returnstring = "Szyfracja zakończona powodzeniem!";
        //    string filepath = path + @"\" + filename + ".enc";
        //    try
        //    {
        //        using (var rijndael_AES = new RijndaelManaged())
        //        {
        //            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
        //            rijndael_AES.BlockSize = 128;
        //            rijndael_AES.Key = pdb.GetBytes(32);
        //            rijndael_AES.IV = pdb.GetBytes(16);
        //            rijndael_AES.Mode = mode;
        //            using (var ms = new MemoryStream())
        //            {
        //                using (var cs = new CryptoStream(ms, rijndael_AES.CreateEncryptor(), CryptoStreamMode.Write))
        //                {
        //                    cs.Write(fileToEncryption, 0, fileToEncryption.Length);
        //                    cs.Close();
        //                    if (!File.Exists(filepath))
        //                    {
        //                        using (var binarywrite = new BinaryWriter(File.Open(filepath, FileMode.Create)))
        //                        {
        //                            binarywrite.Write(ms.ToArray());
        //                            binarywrite.Flush();
        //                            binarywrite.Close();
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        returnstring = ex.Message;
        //    }
        //    return returnstring;
        //}

        public static string EncryptionFile_AES(byte[] fileToEncryption, string key, string path, string filename, CipherMode mode)
        {
            string returnstring = "Szyfracja zakończona powodzeniem!";
            string filepath = path + @"\" + filename + ".enc";
            try
            {
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.BlockSize = 128;
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    encryptor.Mode = mode;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(fileToEncryption, 0, fileToEncryption.Length);
                            cs.Close();
                            if (!File.Exists(filepath))
                            {
                                using (var binarywrite = new BinaryWriter(File.Open(filepath, FileMode.Create)))
                                {
                                    binarywrite.Write(ms.ToArray());
                                    binarywrite.Flush();
                                    binarywrite.Close();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                returnstring = ex.Message;
            }
            return returnstring;
        }

        public static string EncryptionFile_DES(byte[] fileToEncryption, string key, string path, string filename, CipherMode mode)
        {
            string returnstring = "Szyfracja zakończona powodzeniem!";
            string filepath = path + @"\" + filename + ".enc";
            try
            {
                using (var encryptor = DES.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.BlockSize = 64;
                    encryptor.Key = pdb.GetBytes(8);
                    encryptor.IV = pdb.GetBytes(8);
                    encryptor.Mode = mode;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(fileToEncryption, 0, fileToEncryption.Length);
                            cs.Close();
                            if (!File.Exists(filepath))
                            {
                                using (var binarywrite = new BinaryWriter(File.Open(filepath, FileMode.Create)))
                                {
                                    binarywrite.Write(ms.ToArray());
                                    binarywrite.Flush();
                                    binarywrite.Close();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                returnstring = ex.Message;
            }
            return returnstring;
        }
    }
}
