﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Windows;
using System.Windows.Forms;

namespace SzyfrowanieIDeszyfrowanie
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        //proba zmiany
        //to tez

        private void btnEncryptionPanel_Click(object sender, RoutedEventArgs e)
        {
            DecryptionPanel.Visibility = Visibility.Collapsed;
            btnDecryption.Visibility = Visibility.Collapsed;
            EncryptionPanel.Visibility = Visibility.Visible;
            btnEncryption.Visibility = Visibility.Visible;
            labelKey.Content = "Klucz szyfrujący";
        }

        private void btnDecryptionPanel_Click(object sender, RoutedEventArgs e)
        {
            EncryptionPanel.Visibility = Visibility.Collapsed;
            btnEncryption.Visibility = Visibility.Collapsed;
            DecryptionPanel.Visibility = Visibility.Visible;
            btnDecryption.Visibility = Visibility.Visible;
            labelKey.Content = "Klucz deszyfrujący";
        }

        private void btnChooseFileToEncryption_Click(object sender, RoutedEventArgs e)
        {
            using (OpenFileDialog fileencryption = new OpenFileDialog())
            {
                fileencryption.ShowDialog();
                txtboxPathEncryption.Text = fileencryption.FileName;
            }
        }

        private void btnChooseFolderToSaveEncryption_Click(object sender, RoutedEventArgs e)
        {
            using (FolderBrowserDialog folder = new FolderBrowserDialog())
            {
                DialogResult result = folder.ShowDialog();
                txtboxPathSaveFileEncryption.Text = folder.SelectedPath;
            }
        }

        private void btnChooseFileToDecryption_Click(object sender, RoutedEventArgs e)
        {
            using (OpenFileDialog filedecryption = new OpenFileDialog())
            {
                filedecryption.ShowDialog();
                txtboxPathDecryption.Text = filedecryption.FileName;
            }
        }

        private void btnChooseFolderToSaveDecryption_Click(object sender, RoutedEventArgs e)
        {
            using (FolderBrowserDialog folder = new FolderBrowserDialog())
            {
                DialogResult result = folder.ShowDialog();
                txtboxPathSaveFileDecryption.Text = folder.SelectedPath;
            }
        }

        private void btnEncryption_Click(object sender, RoutedEventArgs e)
        {
            if (txtboxPathEncryption.Text != "" && txtboxPathSaveFileEncryption.Text != "" && txtBoxKey.Text != "")
            {
                try
                {
                    if (comboBox.SelectedIndex == 0)
                    {
                        if (comboBoxMode.SelectedIndex == 0)
                        {
                            System.Windows.MessageBox.Show(Encryption.EncryptionFile_AES(File.ReadAllBytes(txtboxPathEncryption.Text), txtBoxKey.Text, txtboxPathSaveFileEncryption.Text, Path.GetFileName(txtboxPathEncryption.Text), CipherMode.CBC));
                        }
                        if (comboBoxMode.SelectedIndex == 1)
                        {
                            System.Windows.MessageBox.Show(Encryption.EncryptionFile_AES(File.ReadAllBytes(txtboxPathEncryption.Text), txtBoxKey.Text, txtboxPathSaveFileEncryption.Text, Path.GetFileName(txtboxPathEncryption.Text), CipherMode.ECB));
                        }
                    }
                    else if (comboBox.SelectedIndex == 1)
                    {
                        if (comboBoxMode.SelectedIndex == 0)
                        {
                            System.Windows.MessageBox.Show(Encryption.EncryptionFile_DES(File.ReadAllBytes(txtboxPathEncryption.Text), txtBoxKey.Text, txtboxPathSaveFileEncryption.Text, Path.GetFileName(txtboxPathEncryption.Text), CipherMode.CBC));
                        }
                        if (comboBoxMode.SelectedIndex == 1)
                        {
                            System.Windows.MessageBox.Show(Encryption.EncryptionFile_DES(File.ReadAllBytes(txtboxPathEncryption.Text), txtBoxKey.Text, txtboxPathSaveFileEncryption.Text, Path.GetFileName(txtboxPathEncryption.Text), CipherMode.ECB));
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
            }
            else
                System.Windows.MessageBox.Show("Wszystkie pola muszą być wypełnione!");
        }

        private void btnDecryption_Click(object sender, RoutedEventArgs e)
        {
            if (txtboxPathDecryption.Text != "" && txtboxPathSaveFileDecryption.Text != "" && txtBoxKey.Text != "")
            {
                try
                {
                    if (comboBox.SelectedIndex == 0)
                    {
                        if (comboBoxMode.SelectedIndex == 0)
                        {
                            System.Windows.MessageBox.Show(Decryption.DecryptionFile_AES(File.ReadAllBytes(txtboxPathDecryption.Text), txtBoxKey.Text, txtboxPathSaveFileDecryption.Text, Path.GetFileNameWithoutExtension(txtboxPathDecryption.Text), CipherMode.CBC));
                        }
                        if (comboBoxMode.SelectedIndex == 1)
                        {
                            System.Windows.MessageBox.Show(Decryption.DecryptionFile_AES(File.ReadAllBytes(txtboxPathDecryption.Text), txtBoxKey.Text, txtboxPathSaveFileDecryption.Text, Path.GetFileNameWithoutExtension(txtboxPathDecryption.Text), CipherMode.ECB));
                        }
                    }
                    else if (comboBox.SelectedIndex == 1)
                    {
                        if (comboBoxMode.SelectedIndex == 0)
                        {
                            System.Windows.MessageBox.Show(Decryption.DecryptionFile_DES(File.ReadAllBytes(txtboxPathDecryption.Text), txtBoxKey.Text, txtboxPathSaveFileDecryption.Text, Path.GetFileNameWithoutExtension(txtboxPathDecryption.Text), CipherMode.CBC));
                        }
                        if (comboBoxMode.SelectedIndex == 1)
                        {
                            System.Windows.MessageBox.Show(Decryption.DecryptionFile_DES(File.ReadAllBytes(txtboxPathDecryption.Text), txtBoxKey.Text, txtboxPathSaveFileDecryption.Text, Path.GetFileNameWithoutExtension(txtboxPathDecryption.Text), CipherMode.ECB));
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
            }
            else
                System.Windows.MessageBox.Show("Wszystkie pola muszą być wypełnione!");
        }
    }
}
